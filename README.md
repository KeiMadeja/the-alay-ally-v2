*The Alay Ally*
- A checklist for optimized grocery shopping.

Submitted by:
- Keisha Madeja
- Kaycee Silva
- Jethro Tejano

Core Function:
A checklist that categorizes your entries to allow for an easier grocery shopping experience.

This app was created for and submitted to Zuitt
for the fulfillment of a Mini-Capstone Activity testing our knowledge of JavaScript.

The requirements for the activity are as follows:

Theme:
An App that can make the world a happier place.

Goal:
We are currently in a unique situation due to COVID-19.
So for your hackathon, create an app that can help individuals, businesses,
organizations, government, etc. to cope up with the challenges of COVID.

Reasoning:
The following are some of the challenges we faced that are relevant to the output we produced:
1. During the ECQ, it was really difficult to have only one member per household allowed
to go out and buy groceries.
2. To have a lesser risk of contracting the virus, it was heavily advised that people
stayed indoors and avoid staying out for prolonged periods of time.
3. This sometimes led to the one member per household being someone who was not used to grocery shopping
and would take an unnecessary amount of time at the grocery store because they were having trouble
finding what they wanted to buy.

Name:
The Alay's Ally is a play on the joke that the one member per household
is often compared to a "tribute" also known as an "alay" who is volunteered to
gather resources for the family. With the Alay's Ally, the one member per household
no longer has to go shopping alone!

Functionality Intent:
Our app aims to differentiate itself from other grocery list apps by offering a
way for users to categorize the items they are adding to their list to have similar items
next to each other on the list. This way when "The Alay" per household goes grocery shopping,
they should have an easier and faster time shopping, which leads to less unnecessary time outdoors, which leads
to lesser risk of catching the virus, and teaches users the value of organizing and preparation.

How to Use the App:
1. List down the item you would like to add to your grocery list.
2. Select the category it falls under.
3. Enter your budget, and your list is made!
4. As you cross off items on your list, make sure to keep track of how much each item costs so you can stay within the budget!

Basic Features:
+ A way to add an item to the list.
+ A way to make sure that each item on the list is categorized.
+ A responsive UI that resembles the aesthetic of a handwritten list.

Additional Features:
+ Make sure that each category only appears once it actually has items listed in it.
+ Make sure the categories are ordered in a suggested order that helps ensure quality of shopping.
(Buy canned goods and nonperishables first and fresh produce and frozen goods last because
during the crisis, canned goods are more likely to run out of stock, while buying fresh and
frozen goods last will ensure that they don't melt or be squished when shopping)
+ A way for the user to cross out the items they've already purchased.
o A way for the user to uncross out the items they've changed their  mind on.
+ A budget tracker.