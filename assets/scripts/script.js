// ===============

const cannedItems = []
const saucesItems = []
const pastasAndRiceItems = []
const bakingItems = []
const snacksItems = []
const beveragesItems = []
const importedItems =[]
const dairyItems = []
const freshItems = []
const frozenItems = []
const othersItems = []


const addToListBtn = document.getElementById("addToListBtn")

const newItem = document.getElementById("addItemSection").firstElementChild.nextElementSibling

const selectCategory = document.getElementById("selectCategory")


addToListBtn.addEventListener("click", function(){
  // Clears all lists to make sure there aren't duplicates.
  document.getElementById("cannedList").innerHTML = "";
  document.getElementById("condimentsSaucesList").innerHTML = "";
  document.getElementById("pastasAndRiceList").innerHTML = "";
  document.getElementById("bakingNeedsList").innerHTML = "";
  document.getElementById("snacksList").innerHTML = "";
  document.getElementById("beveragesList").innerHTML = "";
  document.getElementById("importedList").innerHTML = "";
  document.getElementById("dairyList").innerHTML = "";
  document.getElementById("freshProduceList").innerHTML = "";
  document.getElementById("frozenList").innerHTML = "";
  document.getElementById("othersList").innerHTML = "";

  // Adding Items to the Canned Goods List.
  if(newItem.value !== "" && selectCategory.value === "Canned Goods"){
    cannedItems.push(newItem.value)
  }
  cannedItems.forEach(function(indivItem){
    const newListItem = document.createElement('li');

  newListItem.innerHTML = `<li><span class="checkmark">${indivItem}</span>
                          <span class="undoAndCloseBtns">
                          <span class="undoBtn">&#8630</span>
                          <span class="closeBtn">\u00D7</span>
                          </span>
                          <input type="number" class="form-control itemPrice" placeholder="Input Price">
                          </li>`

  document.getElementById("cannedList").appendChild(newListItem);
  document.getElementById("noItemsDiv").classList.remove("d-flex");
  document.getElementById("noItemsDiv").classList.add("d-none");
  document.getElementById("cannedDiv").classList.remove("d-none");
  })

    // Adding Items to the Condiments / Sauces List.
  if(newItem.value !== "" && selectCategory.value === "Condiments / Sauces"){
    saucesItems.push(newItem.value)
  }
  saucesItems.forEach(function(indivItem){
    const newListItem = document.createElement('li');

 newListItem.innerHTML = `<li><span class="checkmark">${indivItem}</span>
                          <span class="undoAndCloseBtns">
                          <span class="undoBtn">&#8630</span>
                          <span class="closeBtn">\u00D7</span>
                          </span>
                          <input type="number" class="form-control itemPrice" placeholder="Input Price">
                          </li>`

  document.getElementById("condimentsSaucesList").appendChild(newListItem);
  document.getElementById("noItemsDiv").classList.remove("d-flex");
  document.getElementById("noItemsDiv").classList.add("d-none");
  document.getElementById("condimentsDiv").classList.remove("d-none");
  })


    // Adding Items to the Pastas and Rice List.
  if(newItem.value !== "" && selectCategory.value === "Pastas and Rice"){
    pastasAndRiceItems.push(newItem.value)
  }
  pastasAndRiceItems.forEach(function(indivItem){
    const newListItem = document.createElement('li');

 newListItem.innerHTML = `<li><span class="checkmark">${indivItem}</span>
                          <span class="undoAndCloseBtns">
                          <span class="undoBtn">&#8630</span>
                          <span class="closeBtn">\u00D7</span>
                          </span>
                          <input type="number" class="form-control itemPrice" placeholder="Input Price">
                          </li>`

  document.getElementById("pastasAndRiceList").appendChild(newListItem);
  document.getElementById("noItemsDiv").classList.remove("d-flex");
  document.getElementById("noItemsDiv").classList.add("d-none");
  document.getElementById("pastasDiv").classList.remove("d-none");

  })

    // Adding Items to the Baking Needs List.
  if(newItem.value !== "" && selectCategory.value === "Baking Needs"){
    bakingItems.push(newItem.value)
  }
  bakingItems.forEach(function(indivItem){
    const newListItem = document.createElement('li');

 newListItem.innerHTML = `<li><span class="checkmark">${indivItem}</span>
                          <span class="undoAndCloseBtns">
                          <span class="undoBtn">&#8630</span>
                          <span class="closeBtn">\u00D7</span>
                          </span>
                          <input type="number" class="form-control itemPrice" placeholder="Input Price">
                          </li>`

  document.getElementById("bakingNeedsList").appendChild(newListItem);
  document.getElementById("noItemsDiv").classList.remove("d-flex");
  document.getElementById("noItemsDiv").classList.add("d-none");
  document.getElementById("bakingDiv").classList.remove("d-none");
  })

    // Adding Items to the Snacks List.
  if(newItem.value !== "" && selectCategory.value === "Snacks"){
    snacksItems.push(newItem.value)
  }
  snacksItems.forEach(function(indivItem){
    const newListItem = document.createElement('li');

 newListItem.innerHTML = `<li><span class="checkmark">${indivItem}</span>
                          <span class="undoAndCloseBtns">
                          <span class="undoBtn">&#8630</span>
                          <span class="closeBtn">\u00D7</span>
                          </span>
                          <input type="number" class="form-control itemPrice" placeholder="Input Price">
                          </li>`

  document.getElementById("snacksList").appendChild(newListItem);
  document.getElementById("noItemsDiv").classList.remove("d-flex");
  document.getElementById("noItemsDiv").classList.add("d-none");
  document.getElementById("snacksDiv").classList.remove("d-none");
  })

    // Adding Items to the Beverages List.
  if(newItem.value !== "" && selectCategory.value === "Beverages"){
    beveragesItems.push(newItem.value)
  }
  beveragesItems.forEach(function(indivItem){
    const newListItem = document.createElement('li');

 newListItem.innerHTML = `<li><span class="checkmark">${indivItem}</span>
                          <span class="undoAndCloseBtns">
                          <span class="undoBtn">&#8630</span>
                          <span class="closeBtn">\u00D7</span>
                          </span>
                          <input type="number" class="form-control itemPrice" placeholder="Input Price">
                          </li>`

  document.getElementById("beveragesList").appendChild(newListItem);
  document.getElementById("noItemsDiv").classList.remove("d-flex");
  document.getElementById("noItemsDiv").classList.add("d-none");
  document.getElementById("beveragesDiv").classList.remove("d-none");
  })

    // Adding Items to the Imported Items List.
  if(newItem.value !== "" && selectCategory.value === "Imported Items"){
    importedItems.push(newItem.value)
  }
  importedItems.forEach(function(indivItem){
    const newListItem = document.createElement('li');

 newListItem.innerHTML = `<li><span class="checkmark">${indivItem}</span>
                          <span class="undoAndCloseBtns">
                          <span class="undoBtn">&#8630</span>
                          <span class="closeBtn">\u00D7</span>
                          </span>
                          <input type="number" class="form-control itemPrice" placeholder="Input Price">
                          </li>`

  document.getElementById("importedList").appendChild(newListItem);
  document.getElementById("noItemsDiv").classList.remove("d-flex");
  document.getElementById("noItemsDiv").classList.add("d-none");
  document.getElementById("importedDiv").classList.remove("d-none");
  })

    // Adding Items to the Dairy and Cheeses List.
  if(newItem.value !== "" && selectCategory.value === "Dairy and Cheeses"){
    dairyItems.push(newItem.value)
  }
  dairyItems.forEach(function(indivItem){
    const newListItem = document.createElement('li');

 newListItem.innerHTML = `<li><span class="checkmark">${indivItem}</span>
                          <span class="undoAndCloseBtns">
                          <span class="undoBtn">&#8630</span>
                          <span class="closeBtn">\u00D7</span>
                          </span>
                          <input type="number" class="form-control itemPrice" placeholder="Input Price">
                          </li>`

  document.getElementById("dairyList").appendChild(newListItem);
  document.getElementById("noItemsDiv").classList.remove("d-flex");
  document.getElementById("noItemsDiv").classList.add("d-none");
  document.getElementById("dairyDiv").classList.remove("d-none");
  })

    // Adding Items to the Fresh Produce List.
  if(newItem.value !== "" && selectCategory.value === "Fresh Produce"){
    freshItems.push(newItem.value)
  }
  freshItems.forEach(function(indivItem){
    const newListItem = document.createElement('li');

 newListItem.innerHTML = `<li><span class="checkmark">${indivItem}</span>
                          <span class="undoAndCloseBtns">
                          <span class="undoBtn">&#8630</span>
                          <span class="closeBtn">\u00D7</span>
                          </span>
                          <input type="number" class="form-control itemPrice" placeholder="Input Price">
                          </li>`

  document.getElementById("freshProduceList").appendChild(newListItem);
  document.getElementById("noItemsDiv").classList.remove("d-flex");
  document.getElementById("noItemsDiv").classList.add("d-none");
  document.getElementById("freshDiv").classList.remove("d-none");
  })

    // Adding Items to the Frozen Goods List.
  if(newItem.value !== "" && selectCategory.value === "Frozen Goods"){
    frozenItems.push(newItem.value)
  }
  frozenItems.forEach(function(indivItem){
    const newListItem = document.createElement('li');

 newListItem.innerHTML = `<li><span class="checkmark">${indivItem}</span>
                          <span class="undoAndCloseBtns">
                          <span class="undoBtn">&#8630</span>
                          <span class="closeBtn">\u00D7</span>
                          </span>
                          <input type="number" class="form-control itemPrice" placeholder="Input Price">
                          </li>`

  document.getElementById("frozenList").appendChild(newListItem);
  document.getElementById("noItemsDiv").classList.remove("d-flex");
  document.getElementById("noItemsDiv").classList.add("d-none");
  document.getElementById("frozenDiv").classList.remove("d-none");

  })

    // Adding Items to the Others List.
  if(newItem.value !== "" && selectCategory.value === "Others"){
    othersItems.push(newItem.value)
  }
  othersItems.forEach(function(indivItem){
    const newListItem = document.createElement('li');

 newListItem.innerHTML = `<li><span class="checkmark">${indivItem}</span>
                          <span class="undoAndCloseBtns">
                          <span class="undoBtn">&#8630</span>
                          <span class="closeBtn">\u00D7</span>
                          </span>
                          <input type="number" class="form-control itemPrice" placeholder="Input Price">
                          </li>`

  document.getElementById("othersList").appendChild(newListItem);
  document.getElementById("noItemsDiv").classList.remove("d-flex");
  document.getElementById("noItemsDiv").classList.add("d-none");
  document.getElementById("othersDiv").classList.remove("d-none");

  })
})

// Function that allows users to cross out an item on their list.
document.addEventListener("click",function(event){
  if(event.target.classList.contains("checkmark")){
    event.target.classList.add("checked")
  }

  if(event.target.classList.contains("undoBtn")){
    event.target.parentElement.previousElementSibling.classList.remove("checked")
  }

  if(event.target.classList.contains("closeBtn")){
    event.target.parentElement.parentElement.classList.add("d-none")
    itemsBought -= parseFloat(event.target.parentElement.nextElementSibling.value);
    itemsBoughtSpan.textContent = itemsBought;
  }
}) 

// Budget Tracker

let itemsBought = 0
let totalBudget = 0

const itemsBoughtSpan = document.getElementById("itemsBought")

const totalBudgetSpan = document.getElementById("totalBudget")
const totalBudgetInput = document.getElementById("budgetInput")

totalBudgetInput.addEventListener("change", function(){
  totalBudget = parseFloat(
    totalBudgetInput.value);

  totalBudgetSpan.textContent = totalBudget;
})

// Adding Event Listener to check for prices:
document.addEventListener("change",function(event){
  if(event.target.classList.contains('itemPrice')){
    itemsBought += parseFloat(event.target.value);
    itemsBoughtSpan.textContent = itemsBought;
  }
})

// Remove Add to List Function when user starts shopping.

let startShoppingBtn = document.getElementById("startShoppingBtn")

startShoppingBtn.addEventListener("click",function(){
  document.getElementById("addItemSection").classList.add("d-none")
})
